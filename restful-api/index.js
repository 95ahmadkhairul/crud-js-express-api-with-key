const path = require('path');
const express = require('express');
const hbs = require('hbs');
const bodyParser = require('body-parser');
const cors = require('cors');
const mysql = require('mysql');
const app = express();
 
app.set('views',path.join(__dirname,'views'));
app.set('view engine', 'hbs');
app.use('/assets',express.static(__dirname + '/public'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cors());

//create database connection
const conn = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: '',
  database: 'restful_db'
});
 
//connect to database
conn.connect((err) =>{
  if(err) throw err;
});

//show all api key
app.get('/',(req, res) => {
  	let sql = "SELECT * FROM api";
  	let query = conn.query(sql, (err, results) => {
    	if(err) throw err;
		res.render('index',{
      		results: results
    	});
	});
});

//insert new api key
app.post('/save',(req, res) => {
  	let sql = "INSERT INTO api VALUES (NULL, '"+req.body.api_key+"', '"+req.body.api_user+"')";
  	let query = conn.query(sql, (err, results) => {
    	if(err) throw err;
		res.redirect('/');
	});
});

//delete api key
app.get('/delete/:api_id',(req, res) => {
  let sql = "DELETE FROM api WHERE api_id="+req.params.api_id+"";
  let query = conn.query(sql, (err, results) => {
    if(err) throw err;
      res.redirect('/');
  });
});

//get all data product
app.get('/api/products/:api_key',(req, res) => {
  	let sql = "SELECT * FROM api WHERE api_key='"+req.params.api_key+"'";
  	let query = conn.query(sql, (err, results) => {
    	if(err) throw err;
    	if (results.length == 1){
	    	let sql = "SELECT * FROM product";
	  		let query = conn.query(sql, (err, results) => {
	    		if(err) throw err;
	    		res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
	  		});
  		}else{
  			res.send(JSON.stringify({"status": "API Key not found"}));
  		}
  	});
});

//get data product by id 
app.get('/api/products/:api_key/:id',(req, res) => {
	let sql = "SELECT * FROM api WHERE api_key='"+req.params.api_key+"'";
  	let query = conn.query(sql, (err, results) => {
    	if(err) throw err;
    	if (results.length == 1){
	  		let sql = "SELECT * FROM product WHERE product_id="+req.params.id;
	  		let query = conn.query(sql, (err, results) => {
	    		if(err) throw err;
	    		res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
	  		});
	  	}else{
  			res.send(JSON.stringify({"status": "API Key not found"}));
  		}
  	});
});
 
//add new product
app.post('/api/products/:api_key',(req, res) => {
	let sql = "SELECT * FROM api WHERE api_key='"+req.params.api_key+"'";
  	let query = conn.query(sql, (err, results) => {
    	if(err) throw err;
    	if (results.length == 1){
			let data = {product_name: req.body.product_name, product_price: req.body.product_price};
			let sql = "INSERT INTO product SET ?";
			let query = conn.query(sql, data,(err, results) => {
				if(err) throw err;
				res.send(JSON.stringify({"status": 200, "error": null, "response": results}));								
			});
		}else{
  			res.send(JSON.stringify({"status": "API Key not found"}));
  		}
  	});
});
 
//Edit data product berdasarkan id
app.put('/api/products/:api_key/:id',(req, res) => {
	let sql = "SELECT * FROM api WHERE api_key='"+req.params.api_key+"'";
  	let query = conn.query(sql, (err, results) => {
    	if(err) throw err;
    	if (results.length == 1){
			let sql = "UPDATE product SET product_name='"+req.body.product_name+"', product_price='"+req.body.product_price+"' WHERE product_id="+req.params.id;
			let query = conn.query(sql, (err, results) => {
				if(err) throw err;
				res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
			});
		}else{
  			res.send(JSON.stringify({"status": "API Key not found"}));
  		}
  	});
});
 
//Delete data product berdasarkan id
app.delete('/api/products/:api_key/:id',(req, res) => {
	let sql = "SELECT * FROM api WHERE api_key='"+req.params.api_key+"'";
	  	let query = conn.query(sql, (err, results) => {
	    	if(err) throw err;
	    	if (results.length == 1){
				let sql = "DELETE FROM product WHERE product_id="+req.params.id+"";
				let query = conn.query(sql, (err, results) => {
				if(err) throw err;
					res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
				});
			}else{
  			res.send(JSON.stringify({"status": "API Key not found"}));
  		}
  	});
});
 
//Server listening */
app.listen(3000,() =>{
  console.log('Server started on port 3000...');
});